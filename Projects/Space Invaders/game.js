var gBackground = document.getElementById('canvas_background').getContext('2d');
var gPlayer = document.getElementById('canvas_player').getContext('2d');
var gEnemies = document.getElementById('canvas_enemies').getContext('2d');
var gCovers = document.getElementById('canvas_covers').getContext('2d');
var GUI = document.getElementById('canvas_ui').getContext('2d');
var imgSprite = new Image();
imgSprite.src = 'images/invaders.png';

document.addEventListener('keydown', checkKeyDown, false);
document.addEventListener('keyup', checkKeyUp, false);
imgSprite.addEventListener('load', init, false);

var gameState;

var width = 300;
var height = 400;
var FPS = 60;

var stars = [];
var bullets = [];
var enemyBullets = [];
var enemies = [];
var covers = [];
// enemy array size
var rows = 8; 
var cols = 8;

var shootTimer = 0;
var maxShootTimer = 20;

var score = 0;

var waveNumber;
var enemyShootTimer = 500;
var playerHealth = 10;

// Helper/utylity functions;
var Key = {
	up: false,
	down: false,
	left: false,
	right: false,
	spacebar: false,
	enter: false
};

function collision(obj1, obj2){
	return(
		obj1.x < obj2.x + obj2.width &&
		obj1.x + obj1.width > obj2.x &&
		obj1.y < obj2.y + obj2.height &&
		obj1.y + obj1.height > obj2.y
	);
}

var player = {
	x: (width / 2) - 8,
	y: height - 25,
	width: 16,
	height: 16,
	speed: 3,
	canShoot: true,
	
	render: function(){
		gPlayer.drawImage(imgSprite,64, 0, this.width, this.height, this.x, this.y, this.width, this.height);
	},
	
	tick: function(){
		if(Key.left && this.x > 0)
			this.x -= this.speed;
		if(Key.right && this.x < width - 16)
			this.x += this.speed;
		if(Key.spacebar && this.canShoot){
			this.canShoot = false;
			bullets.push(new Bullet(this.x, this.y - 4, null));
			bullets.push(new Bullet(this.x + this.width, this.y - 4, null));
			shootTimer = maxShootTimer;
		}
	}
};

function Star(x, y){
	this.x = x;
	this.y = y;
	this.size = Math.floor(Math.random() * 3);
	
	this.render = function(){
		gBackground.fillStyle = "white";
		gBackground.fillRect(this.x, this.y, this.size, this.size);
	};
	
	this.tick = function(){
		this.y++;
		if(this.y > 400){
			var index = stars.indexOf(this);
			if (index > -1) {
				stars.splice(index, 1);
			}
		}
	};
}

function createStars(amount){
	for(var i = 0; i < amount; i++){
		stars.push(new Star(Math.random() * width, Math.random() * height - 30));
	}
}

function initStars(amount){
	for(var i = 0; i < amount; i++){
		stars.push(new Star(Math.random() * width, Math.random() * height));
	}
}

function Bullet(x, y, fof){
	
	this.fof = fof; //friend(player) or foe(enemy) flag
	this.x = x;
	this.y = y;
	this.speed = 4;
	this.width = 2;
	this.height = 12;
	
	this.render = function(){
		gPlayer.fillStyle = "yellow";
		gPlayer.fillRect(this.x, this.y, this.width, this.height);
	};
	
	this.tick = function(){
	if(fof == null){
		this.y -= this.speed;
		if(this.y < -this.height){
			var index = bullets.indexOf(this);
			if (index > -1) {
				bullets.splice(index, 1);
			}
		}
		
		//Collision detection player - enemy:	
		for(i in enemies){
			if(collision(this, enemies[i])){
				var enemyIndex = enemies.indexOf(enemies[i]);
				enemies[i].health--;
				if(enemies[i].health <= 0){
					score = score + enemies[i].type;
					enemies.splice(enemyIndex, 1);
				}
				var bulletIndex = bullets.indexOf(this);
				bullets.splice(bulletIndex, 1);	
				
			}
		}
		//Collision detection player - covers:
		for(i in covers){
			if(collision(this, covers[i])){
				var bulletIndex = bullets.indexOf(this);
				bullets.splice(bulletIndex, 1);	
			}
		}
	}
		//Collision detection enemy - player:	
		if(fof != null){
			this.y += this.speed;
		
			if(this.y >= height){
				var index = enemyBullets.indexOf(this);
				if (index > -1) {
					enemyBullets.splice(index, 1);
				}
			}	
				
			if(collision(this, player)){
				playerHealth--;
				if(playerHealth <= 0){
					gameState = "gameOver";
				}
				var bulletIndex = enemyBullets.indexOf(this);
				enemyBullets.splice(bulletIndex, 1);
			}
			

			for(i in covers){
				if(collision(this, covers[i])){
				covers[i].health--;
				if(covers[i].health <= 0){
					var coverIndex = covers.indexOf(covers[i]);
					covers.splice(coverIndex, 1);
				}
				var bulletIndex = enemyBullets.indexOf(this);
				enemyBullets.splice(bulletIndex, 1);	
				}
			}
		}
		
		
	};
}

function Cover(x, y){
	this.x = x;
	this.y = y;
	this.health = 10;
	this.width = 32;
	this.height = 10;

	this.render = function(){
		gCovers.fillStyle = "DarkGreen"; 
		gCovers.fillRect(this.x, this.y, this.width, this.health);
	};
	
	this.tick = function(){
		
	};
}

function initCovers(amount){

	for(var i = 0; i < amount; i++){
		covers.push(new Cover((width/amount)*i + 30, 340));
	}
}

function Enemy(x, y, type){
	this.x = x;
	this.y = y;
	this.type = type;
	this.health = type;
	this.width = 16;
	this.height = 16;
	this.canShoot = false;
	
	this.speed = 0.5;
	
	this.render = function(){
		if (type == 1){
			gEnemies.drawImage(imgSprite,0, 0, this.width, this.height, this.x, this.y + 35, this.width, this.height);
		}
		else if(type == 2){
			gEnemies.drawImage(imgSprite,16, 0, this.width, this.height, this.x, this.y + 35, this.width, this.height);
		}
		else if(type == 3){
			gEnemies.drawImage(imgSprite,32, 0, this.width, this.height, this.x, this.y + 35, this.width, this.height);
		}
		else if(type == 4){
			gEnemies.drawImage(imgSprite,48, 0, this.width, this.height, this.x, this.y + 35, this.width, this.height);
		}
	};
	
	this.tick = function(){
				
		this.x += this.speed;
		
		var firstEnemy = enemies[0];

		if(firstEnemy.x <= 0){
			this.speed = -this.speed;
		}

		if(firstEnemy.x + 185 >= width){
			this.speed = -this.speed;
		}

		if (enemyShootTimer == 1){
			this.canShoot = true;
		}
		if(firstEnemy == null){
			firstEnemy = enemies[0];
		}
		var randomNumber = Math.floor(Math.random()*1000);
		 
		if(this.canShoot && randomNumber == 7){
			enemyBullets.push(new Bullet(this.x, this.y, "test"));
			this.canShoot = false;
			enemyShootTimer == 500;
		}
	};
}

function createNewEnemies() {
		enemies.length = 0

		if (waveNumber == 2) {
			for(var i=0; i < rows; i++){
				for(var j=0; j < cols; j++){
					if (j < 4){
						enemies.push(new Enemy(i*24, j*24, 1));
					}
					else if (j < 7){
						enemies.push(new Enemy(i*24, j*24, 2));
					}
					else{
						enemies.push(new Enemy(i*24, j*24, 3));
					}
				}
			}
		}

		if (waveNumber ==  3) {
			for(var i=0; i < rows; i++){
				for(var j=0; j < cols; j++){
					if (j < 3){
						enemies.push(new Enemy(i*24, j*24, 1));
					}
					else if (j < 5){
						enemies.push(new Enemy(i*24, j*24, 2));
					}
					else if (j < 7){
						enemies.push(new Enemy(i*24, j*24, 3));
					}
					else{
						enemies.push(new Enemy(i*24, j*24, 4));
					}
				}
			}
		}
		if (waveNumber ==  4){
			gameState = "win";
		}
};

function init(){
	gameState = "playing";
	initStars(60);
	 for(var i=0; i < rows; i++){
		for(var j=0; j < cols; j++){
			if (j < 6){
				enemies.push(new Enemy(i*24, j*24, 1));
			}
			else if (j < 9){
				enemies.push(new Enemy(i*24, j*24, 2));
			}
		}
	}
	initCovers(3);
	waveNumber = 1;	
}

init();

function render(){
	
	gBackground.clearRect(0, 0, width, height);
	gPlayer.clearRect(0, 0, width, height);
	gEnemies.clearRect(0, 0, width, height);
	gCovers.clearRect(0, 0, width, height);
	GUI.clearRect(0, 0, width, height);
	
	for(i in stars){
		stars[i].render();
	}

	if (gameState == "playing"){
		if (playerHealth <= 0){
			gameState = "gameOver"
		}		
		player.render();
		
		for(i in enemies){
			enemies[i].render();
		}

		for(i in bullets){
			bullets[i].render();
		}
		for(i in enemyBullets){
			enemyBullets[i].render();
		}
		for(i in covers){
			covers[i].render();
		}
		
		if (gameState == "playing"){	
			GUI.fillStyle = "white";
			GUI.textBaseline = "top";
			GUI.font = "14px Verdana";
			GUI.fillText("Score: " + score, 2, 1);
			GUI.fillText("Health: " + playerHealth, 2, 16);
			GUI.fillText("wave: " + waveNumber, 2, 380);
		}
	}	
	else if (gameState == "gameOver"){
		GUI.fillStyle = "white";
		GUI.textBaseline = "top";
		GUI.font = "26px Verdana";
		GUI.fillText("GAME OVER", width/2 - 75, height/2 - 13);
		GUI.font = "16px Verdana";
		GUI.fillText("Press enter to play again.", width/2 - 95, height/2 +30);
	}
	
	else if (gameState == "win"){
		GUI.fillStyle = "white";
		GUI.textBaseline = "top";
		GUI.font = "24px Verdana";
		GUI.fillText("You saved the galaxy!", width/2 - 130, height/2 - 13);
		GUI.font = "16px Verdana";
		GUI.fillText("Press enter to play again.", width/2 - 110, height/2 + 30);
	}
	
}

function tick(){
	for (i in stars){
		stars[i].tick();
	}
	
	createStars(1);
	
	if (gameState == "playing"){
		player.tick();
		shootTimer--;
		enemyShootTimer--;

		if(enemyShootTimer == 0){
			enemyShootTimer = 500;
		}
		for(i in enemies){
			enemies[i].tick();
		}
		
		for(i in bullets){
			bullets[i].tick();
		}

		for(i in enemyBullets){
			enemyBullets[i].tick();
		}

		if(shootTimer <= 0){
			player.canShoot = true;
		}
		
		if (enemies.length == 0) {
			waveNumber++;
			createNewEnemies();
			covers = [];
			initCovers(3);
		}
	}
	else if(gameState == "gameOver" || gameState == "win"){
		if(Key.enter){
			location.reload();
		}
	}
}

// Game loop:
setInterval (function(){
	render();
	tick();
}, 1000/FPS);


//Start: Key Handling
function checkKeyDown(e){
	var keyCode = e.keyCode || e.which;
	if (keyCode == 38){
		Key.up = true;
		e.preventDefault();
	}
	
	if (keyCode == 40){
		Key.down = true;
		e.preventDefault();
	}
	
	if (keyCode == 37){
		Key.left = true;
		e.preventDefault();
	}
	
	if (keyCode == 39){
		Key.right = true;
		e.preventDefault();
	}
	
	if (keyCode == 32){
		Key.spacebar = true;
		e.preventDefault();
	}
	
	if (keyCode == 13){
		Key.enter = true;
		e.preventDefault();
	}
}

function checkKeyUp(e){
	var keyCode = e.keyCode || e.which;
	
	if (keyCode == 38){
		Key.up = false;
		e.preventDefault();
	}
	
	if (keyCode == 40){
		Key.down = false;
		e.preventDefault();
	}
	
	if (keyCode == 37){
		Key.left = false;
		e.preventDefault();
	}
	
	if (keyCode == 39){
		Key.right = false;
		e.preventDefault();
	}
		if (keyCode == 32){
		Key.spacebar = false;
		e.preventDefault();
	}
	
		if (keyCode == 13){
		Key.enter = false;
		e.preventDefault();
	}
}
//End: Key Handling
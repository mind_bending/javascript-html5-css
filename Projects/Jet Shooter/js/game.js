var canvasBg = document.getElementById('canvasBg');
var ctxBg = canvasBg.getContext('2d');

// Jet canvas
var canvasEnemy = document.getElementById('canvasEnemy');
var ctxEnemy = canvasEnemy.getContext('2d');

// Enemy canvas
var canvasJet = document.getElementById('canvasJet');
var ctxJet = canvasJet.getContext('2d');

// HUD canvas
var canvasHUD = document.getElementById('canvasHUD');
var ctxHUD = canvasHUD.getContext('2d');
ctxHUD.fillStyle = "#000000"; 
ctxHUD.font = "bold 20px CenturyGothic";



var jet1;
var btnPlay = new Button(265, 535, 220, 335);
var gameWidth = canvasBg.width;
var gameHeight = canvasBg.height;
var mouseX = 0;
var mouseY = 0;
var counter = 0;
var isPlaying = false;
//Cross browser support:
var requestAnimFrame = window.requestAnimationFrame ||
						window.webkitRequestAnimationFrame ||
						window.mozRequestAnimationFrame ||
						window.msRequestAnimationFrame ||
						window.oRequestAnimationFrame;

var enemies = [];
var spawnAmount = 4;

var imgSprite = new Image();
imgSprite.src = 'images/sprite.png';
imgSprite.addEventListener('load', init, false);

var timer;

//Moving background
var bgDrawX1 = 0;
var bgDrawX2 = 1575;

function moveBg(){
	bgDrawX1 -=5;
	bgDrawX2 -=5;
	
	if(bgDrawX1 <= -1575){
		bgDrawX1 = 1575;
	}
	else if(bgDrawX2 <= -1575){
		bgDrawX2 = 1575;
	}
	drawBg();
}

//End of Moving background

//Key game functionalities
function init(){
	jet1 = new Jet();
	spawnEnemy(spawnAmount);
	drawMenu();
	document.addEventListener('click', mouseClicked, false);
}

function playGame(){

	drawBg();
	startLoop();
	updateHUD();
	document.addEventListener('keydown', checkKeyDown, false);
	document.addEventListener('keyup', checkKeyUp, false);
}


function spawnEnemy(number){
	for (var i = 0; i < number; i++){
		enemies[enemies.length] = new Enemy();
	}
}

function drawAllEnemies(){
	clearCtxEnemy();
	for (var i = 0; i <enemies.length; i++){
		enemies[i].draw();
	}
}


function loop(){
	if (isPlaying){
	  timer = new Date().getTime();
	  moveBg();
	  jet1.draw();
	  drawAllEnemies();
	  requestAnimFrame(loop, 200);
	}
}

function startLoop(){
	isPlaying = true;
	loop();
}

// sleep(ms)
function pausecomp(millis)
 {
  var date = new Date();
  var curDate = null;
  do { curDate = new Date(); }
  while(curDate-date < millis);
}


function stopLoop(){
	isPlaying = false;
	ctxHUD.fillText("GAME OVER", gameWidth/2 - 50, gameHeight/2);
	btnPlay = null;
	
	var buttonLocation = document.getElementById('startAgain');
	var btn = document.createElement("BUTTON");
	btn.onclick = function() { 
        location.reload();
    };
    var t = document.createTextNode("Play Again");
    btn.appendChild(t);
	buttonLocation.appendChild(btn);
}

function drawBg(){
	var srcX = 0;
	var srcY = 0;
	var drawX = 0;
	var drawY = 0;
	ctxBg.clearRect(0, 0, gameWidth, gameHeight);
	ctxBg.drawImage(imgSprite,srcX,srcY,1575,gameHeight,bgDrawX1,drawY,1575,gameHeight);
	ctxBg.drawImage(imgSprite,srcX,srcY,1575,gameHeight,bgDrawX2,drawY,1575,gameHeight);
}

function drawMenu(){
	var srcX = 0;
	var srcY = 580;
	var drawX = 0;
	var drawY = 0;
	ctxBg.drawImage(imgSprite,srcX,srcY,gameWidth,gameHeight,drawX,drawY,gameWidth,gameHeight);
	ctxBg.font = "14px CenturyGothic";
	ctxBg.fillText("Artur Wieczorek", 680, 490);
}

function updateHUD(){
	ctxHUD.clearRect(0, 0, gameWidth, gameHeight);
	ctxHUD.fillText("Score: " + jet1.score, 680, 35);
}

//End of key game functionalities



// Jet: object + function definitions

function Jet(){
	this.srcX = 0;
	this.srcY = 500;
	
	this.width = 100;
	this.height = 40;
	
	this.drawX = 220;
	this.drawY = 200;
	
	//Jet tip
	this.noseX = this.drawX + 100;
	this.noseY = this.drawY + 30;
	
	this.leftX = this.drawX;
	this.rightX = this.drawX + this.width;
	this.topY = this.drawY;
	this.bottomY = this.drawY + this.height;
	
	this.speed = 5;
	
	this.isUpKey = false;
	this.isRightKey = false;
	this.isDownKey = false;
	this.isLeftKey = false;
	this.isSpacebar = false;
	this.isShooting = false;
	this.bullets = [];
	this.currentBullet = 0;
	for(var i = 0; i < 20; i++){
		this.bullets[this.bullets.length] = new Bullet(this, null);
	}
	this.explosion = new Explosion();
	this.score = 0;
}

Jet.prototype.draw = function(){
	clearCtxJet();
	this.updateBoundaries();
	this.checkDirection();
	this.checkShooting();
	this.drawAllBullets();
	this.checkEnemyCrash();
	
	ctxJet.drawImage(imgSprite,this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
};

Jet.prototype.updateBoundaries = function(){

	//Jet tip
	this.noseX = this.drawX + 100;
	this.noseY = this.drawY + 30;
	
	this.leftX = this.drawX;
	this.rightX = this.drawX + this.width;
	this.topY = this.drawY;
	this.bottomY = this.drawY + this.height;
}

Jet.prototype.checkDirection = function(){
	if(this.isUpKey && this.topY > 0){
		this.drawY -= this.speed;
	}
	if(this.isRightKey && this.rightX < gameWidth){
		this.drawX += this.speed;
	}
	if(this.isDownKey && this.bottomY < gameHeight){
		this.drawY += this.speed;
	}
	if(this.isLeftKey && this.leftX > 0){
		this.drawX -= this.speed;
	}
};


Jet.prototype.drawAllBullets = function(){
	for (var i = 0; i <this.bullets.length; i++){
		if(this.bullets[i].drawX >= 0){
			this.bullets[i].draw();
		}
		
		if(this.bullets[i].explosion.hasHit){
			this.bullets[i].explosion.draw();
		}
	}
};

Jet.prototype.checkShooting = function(){
	if(this.isSpacebar && !this.isShooting){
		this.isShooting = true;
		this.bullets[this.currentBullet].fire(this.noseX, this.noseY);
		this.currentBullet++
		if(this.currentBullet >= this.bullets.length){
			this.currentBullet = 0;
		}
	}
	else if(!this.isSpacebar){
		this.isShooting = false;
	}
};

Jet.prototype.updateScore = function(points){
	this.score += points;
	updateHUD();
};

Jet.prototype.checkEnemyCrash = function(){
	for (var i = 0; i <enemies.length; i++){
		if(this.drawX + 90 >= enemies[i].drawX &&
		   this.drawX <= enemies[i].drawX + enemies[i].width &&
		   this.drawY >= enemies[i].drawY &&
		   this.drawY <= enemies[i].drawY + enemies[i].height
		   ){
				this.explosion.drawX = enemies[i].drawX - (this.explosion.width/2);
				this.explosion.drawY = enemies[i].drawY;
				this.explosion.hasHit = true;
				this.explosion.draw();
				ctxJet.globalAlpha = 0;
				ctxJet.drawImage(imgSprite,this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
				
				stopLoop();
		}
	}
};

function clearCtxJet(){
	ctxJet.clearRect(0, 0, gameWidth, gameHeight);
}

// End of Jet: object + function definitions




// Bullet: object + functions

function Bullet(j, e){
	this.jet = j;
	this.enemy = e;
	this.srcX = 100;
	this.srcY = 500;
	
	this.width = 5;
	this.height = 5;
	
	if(this.enemy == null){
		this.drawX = 30;
	}
	else{
		this.drawX = 30;
	}
	this.drawY = 0;
	
	this.explosion = new Explosion();
}

Bullet.prototype.draw = function(){

	if(this.enemy != null){
		this.drawX -= 5;
	}
	else if (this.enemy == null && jet1.isRightKey == true){
		this.drawX += (jet1.drawX/100 + 5);
	}
	else if (this.enemy == null){
		this.drawX += 5;
	}
	ctxJet.drawImage(imgSprite,this.srcX, this.srcY, this.width, this.height, this.drawX + 10, this.drawY - 10, this.width, this.height);
	this.checkHitEnemy();
	this.checkHitPlayer();
	if(this.drawX > gameWidth){
		this.recycle();
	}
};

Bullet.prototype.fire = function(startX, startY){
	this.drawX = startX;
	this.drawY = startY;
};

Bullet.prototype.checkHitEnemy = function(){
	for (var i = 0; i <enemies.length; i++){
		if(this.drawX >= enemies[i].drawX &&
		   this.drawX <= enemies[i].drawX + enemies[i].width &&
		   this.drawY >= enemies[i].drawY &&
		   this.drawY <= enemies[i].drawY + enemies[i].height
		   ){
				this.explosion.drawX = enemies[i].drawX - (this.explosion.width/2);
				this.explosion.drawY = enemies[i].drawY;
				this.explosion.hasHit = true;		
				this.recycle();
				enemies[i].recycleEnemy();
				this.jet.updateScore(enemies[i].rewardPoints);
		}
	}
};

Bullet.prototype.checkHitPlayer = function(){
	
		if(this.drawX >= this.jet.drawX &&
		   this.drawX <= this.jet.drawX + this.jet.width &&
		   this.drawY >= this.jet.drawY &&
		   this.drawY <= this.jet.drawY + this.jet.height
		   ){
				this.explosion.drawX = this.jet.drawX + (this.explosion.width/2);
				this.explosion.drawY = this.jet.drawY;
				this.explosion.hasHit = true;		
				
				stopLoop();
		}
	
};

Bullet.prototype.recycle = function(){
	this.drawX = -20;
};

// End of Bullet: object + functions




// Explosion: object + functions
function Explosion(){
	this.srcX = 750;
	this.srcY = 500;
	
	this.width = 50;
	this.height = 50;
	
	this.drawX = 0;
	this.drawY = 0;
	
	this.hasHit = false;
	this.currentFrame = 0;
	this.totalFrames = 10;
}

Explosion.prototype.draw = function(){
	if(this.currentFrame <= this.totalFrames){
		//draw explosion
		ctxJet.drawImage(imgSprite,this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
		this.currentFrame++;
	}
	else{
		this.hasHit = false;
		this.currentFrame = 0;
	}
};


// End of Explosion: object + functions



// Enemy: object + function definitions
function Enemy(){
	this.srcX = 0;
	this.srcY = 540;
	
	this.width = 100;
	this.height = 40;
	
	this.drawX = Math.floor(Math.random()* 1000) + gameWidth;
	this.drawY = Math.floor(Math.random() * 360);
	
	this.speed = 2;
	
	this.currentBullet = 0;
	this.bullets = [];
	this.enemyShootTimer = 60;
	for(var i = 0; i < 10; i++){
		this.bullets[this.bullets.length] = new Bullet(jet1, this);
	}
	
	this.rewardPoints = 5;
	this.isShooting = true;
}

Enemy.prototype.draw = function(){
	this.drawX -= this.speed;
	ctxEnemy.drawImage(imgSprite,this.srcX, this.srcY, this.width, this.height, this.drawX, this.drawY, this.width, this.height);
	this.enemyShootTimer--;
		if(this.enemyShootTimer == 0){
			this.isShooting = true;
			this.enemyShootTimer = 60;
		}
	
	this.checkShooting();
	this.drawAllBullets();
	this.checkEscaped();
};

Enemy.prototype.drawAllBullets = function(){
	for (var i = 0; i <this.bullets.length; i++){
		if(this.bullets[i].drawX <= 750){
			this.bullets[i].draw();
		}
		
		if(this.bullets[i].explosion.hasHit){
			this.bullets[i].explosion.draw();
		}
	}
};

Enemy.prototype.checkShooting = function(){
	if(this.isShooting){
		this.bullets[this.currentBullet].fire(this.drawX, this.drawY + 20);
		console.log("bullet shot");
		this.currentBullet++
		if(this.currentBullet >= this.bullets.length){
			this.currentBullet = 0;
		}
		this.isShooting = false;
	}
	
};

Enemy.prototype.checkEscaped = function(){
	if (this.drawX + this.width <= 0){
		this.recycleEnemy();
	} 

};

Enemy.prototype.recycleEnemy = function(){
	this.drawX = Math.floor(Math.random()* 1000) + gameWidth;
	this.drawY = Math.floor(Math.random() * 360);
};

function clearCtxEnemy(){
	ctxEnemy.clearRect(0, 0, gameWidth, gameHeight);
}

// End of Enemy: object + function definitions



// Button object

function Button(xL, xR, yT, yB){
	this.xLeft = xL;
	this.xRight = xR;
	this.yTop = yT;
	this.yBottom = yB;
}

Button.prototype.checkClicked = function(){
	if(this.xLeft <= mouseX && mouseX <= this.xRight && this.yTop <= mouseY && mouseY <= this.yBottom){
		return true;
	}
};
// End of Button object



//Events Handling
function mouseClicked(e){
	mouseX = e.pageX - canvasBg.offsetLeft;
	mouseY = e.pageY - canvasBg.offsetTop;	
	if(!isPlaying){
		if(btnPlay != null){
			if(btnPlay.checkClicked()){
				playGame();
			}
		}
	}
}

function checkKeyDown(e){
	var keyID = e.keyCode || e.which;
	
	if (keyID == 38 ||keyID == 87){
		jet1.isUpKey = true;
		e.preventDefault();
	}
	
		if (keyID == 39 ||keyID == 68){
		jet1.isRightKey = true;
		e.preventDefault();
	}
	
		if (keyID == 40 ||keyID == 83){
		jet1.isDownKey = true;
		e.preventDefault();
	}
	
		if (keyID == 37 ||keyID == 65){
		jet1.isLeftKey = true;
		e.preventDefault();
	}
	
		if (keyID == 32){
		jet1.isSpacebar = true;
		e.preventDefault();
	}
}

function checkKeyUp(e){
	var keyID = e.keyCode || e.which;
	
	if (keyID == 38 ||keyID == 87){
		jet1.isUpKey = false;
		e.preventDefault();
	}
	
		if (keyID == 39 ||keyID == 68){
		jet1.isRightKey = false;
		e.preventDefault();
	}
	
		if (keyID == 40 ||keyID == 83){
		jet1.isDownKey = false;
		e.preventDefault();
	}
	
		if (keyID == 37 ||keyID == 65){
		jet1.isLeftKey = false;
		e.preventDefault();
	}
	
		if (keyID == 32){
		jet1.isSpacebar = false;
		e.preventDefault();
	}
}

//End of Events Handling
var canvas = new Object();
var mainSnake;
var food;


canvas.element = document.getElementById('canvas');
canvas.context = canvas.element.getContext('2d');
canvas.width = canvas.element.getAttribute('width');
canvas.height = canvas.element.getAttribute('height');
canvas.cellWidth = 10;

canvas.redraw = function(fillColor, strokeColor){

	var fillColor = fillColor || 'white',
		strokeColor = strokeColor || 'black';

	this.paint(0, 0, fillColor, strokeColor, this.width, this.height);
}

canvas.paint = function(x, y, fillColor, strokeColor, width, height) {
	var width = width || this.cellWidth,
		height = height || this.cellWidth,
		fillColor = fillColor || 'green',
		strokeColor = strokeColor || 'white';

	this.context.fillStyle = fillColor;
	this.context.fillRect(x*canvas.cellWidth, y*canvas.cellWidth, width, height);
	this.context.strokeStyle = strokeColor;
	this.context.strokeRect(x*canvas.cellWidth, y*canvas.cellWidth, width, height);
};

canvas.paintText = function(text, x, y, fillColor, font) {
	var x = x || 10,
		y = y || 15,
		fillColor = fillColor || 'black',
		font = font || "16px CenturyGothic";
	this.context.font = font;
	this.context.fillStyle = fillColor;
	this.context.fillText(text, x, y);
};

canvas.redraw();


function Snake(length, bodyColor, outlineColor, startingPos) {
	this.length = length;
	this.bodyColor = bodyColor;
	this.outlineColor = outlineColor;
	this.array = [];
	this.direction = 'right';
	this.nd = []; // Next direction
	this.nx; // Next X position
	this.ny; // Next Y position
	
	var startingPos = startingPos;
	this.create = function(){
		for(var i = this.length-1; i>=0; i--) {
				this.array.push({x: startingPos.x + i, y: startingPos.y});
			
		}
	};
	this.create();
}

Snake.prototype.move = function() {
	if (this.nd.length) {
		this.direction = this.nd.shift();
	}

	this.nx = this.array[0].x;
	this.ny = this.array[0].y;
	var tail;

	switch(this.direction) {
		case 'right':
			this.nx++;
			break;
		case 'left':
			this.nx--;
			break;
		case 'up':
			this.ny--;
			break;
		case 'down':
			this.ny++;
			break;
	}

	if(this.isOutsideBounds() || this.isColliding()) {
		game.over();
		return;
	}

	if(this.isEatingFood()) {
		game.score++;
		tail = {x: this.nx, y: this.ny};
		food = new Food();
	} else {
		var tail = this.array.pop();
		tail.x = this.nx;
		tail.y = this.ny;
	}

	this.array.unshift(tail);

	this.paint();
}

Snake.prototype.paint = function() {
	canvas.redraw();
	for(var i = 0; i < this.array.length; i++) {
		
		var currentElement = this.array[i];
		if(i == 0){
			var headColor = 'orange';
			canvas.paint(currentElement.x, currentElement.y, headColor, this.outlineColor);
		}
		else{
			canvas.paint(currentElement.x, currentElement.y, this.bodyColor, this.outlineColor);
		}
	}
}

Snake.prototype.isOutsideBounds = function() {
	if(this.nx <= -1 || this.nx === canvas.width/canvas.cellWidth || this.ny <= -1 || this.ny === canvas.height/canvas.cellWidth) {
		return true;
	}
	return false;
}

Snake.prototype.isEatingFood = function() {
	if(this.nx === food.x && this.ny === food.y) {
		return true;
	}
	return false;
}

Snake.prototype.isColliding = function(x, y) {

	var x = x || this.nx,
		y = y || this.ny;
	for(var i = 0; i < this.array.length; i++) {
		if(this.array[i].x === x && this.array[i].y === y) {
			
			return true;
		}
	}
	return false;
}

function Food() {
	this.generateCoordinates = function() {
		this.x = Math.round(Math.random() * (canvas.width-canvas.cellWidth)/canvas.cellWidth);
		this.y = Math.round(Math.random() * (canvas.height-canvas.cellWidth)/canvas.cellWidth);
		this.checkCollision();
	};
	this.checkCollision = function() {
		if(mainSnake.isColliding(this.x, this.y)) {
			this.generateCoordinates();
		}
	};
	this.draw = function(){
		canvas.paint(this.x, this.y, 'red');
	};

	this.generateCoordinates();
	this.checkCollision();
	this.draw();

}

var game = new Object();
game.fps = 20;
game.score = 0;
game.scoreText = 'Score: ';
game.drawScore = function() {
	canvas.paintText(this.scoreText + this.score , 15, 490);
};
game.runLoop = function(){
	setTimeout(function() {
        requestAnimationFrame(game.runLoop);
		mainSnake.move();
		if(typeof food.draw != 'undefined') {
			food.draw();
		}
		game.drawScore();
    }, 1000 / game.fps);
};
game.start = function() {
	mainSnake = new Snake(10, 'green', 'white', {x: 5, y: 5});
	food = new Food();
	game.score = 0;
};
game.over = function(){
	canvas.redraw();
	this.start();
};

game.start();
game.runLoop();


document.onkeydown = function(e) {
	if(typeof mainSnake !== 'undefined'){

		var key = (e.keyCode ? e.keyCode : e.which);
		var td;
		if (mainSnake.nd.length) {
			td = mainSnake.nd[mainSnake.nd.length - 1];
		} else {
			td = mainSnake.direction;
		}
		if(key == "37" && mainSnake.direction != 'right') {
			mainSnake.nd.push('left');
		} else if(key == "38" && mainSnake.direction != 'down') {
			mainSnake.nd.push('up');
		} else if(key == "39" && mainSnake.direction != 'left') {
			mainSnake.nd.push('right');
		} else if(key == "40" && mainSnake.direction != 'up') {
			mainSnake.nd.push('down');
		}
	}
}


 
var lastTime = 0;
var vendors = ['ms', 'moz', 'webkit', 'o'];
for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
                               || window[vendors[x]+'CancelRequestAnimationFrame'];
}
 
if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
}
 
if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
}